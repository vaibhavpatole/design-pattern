import React from 'react'
import Navbar from './components/Navbar'
import Home from './components/Home'
import Show from './components/Show'
import ShowSummary from './ShowSummary'
import { BrowserRouter as Router, Switch,Route } from 'react-router-dom'

function Container() {
  return (
    <div>
      <Router>
        <div className="App">
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/show" exact component={Show} />
          <Route path="/show/:id" component={ShowSummary} />
        </Switch>
        </div>
      </Router>
    </div>
  )
}

export default Container
