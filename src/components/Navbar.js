import React from 'react'
import {Link} from 'react-router-dom'

function Navbar() {
  return (
    <nav className="nav">
      <ul className="nav-lists">
        <Link className="link" to="/">
          <li>Home</li>
        </Link>
        <Link className="link" to="/show">
          <li>Show</li>
        </Link>
      </ul>
    </nav>
  )
}

export default Navbar