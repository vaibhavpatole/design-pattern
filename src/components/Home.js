import React, {useState, useEffect} from 'react'

function Home() {
  useEffect(() => {
    fetchShows();
  },[])

  const [shows, setShows] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(20);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const fetchShows = async () => {
    const data = await fetch('http://api.tvmaze.com/shows?page=1');
    const showList = await data.json();
    console.log(showList);
    setShows(showList);
  }

  const indexLastPage = currentPage * postsPerPage;
  const indexFirstPage = indexLastPage - postsPerPage;
  const currentShow = shows.slice(indexFirstPage,indexLastPage
    );
  return (
    <div>
      {currentShow.map(s => (
        <div key={s.id}>{s.name}</div>))}

    </div>
  )
}

export default Home