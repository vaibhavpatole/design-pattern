import { createActions, createReducer } from "reduxsauce";
export const { Types, Creators } = createActions({
  fetchShow: ["url"],
  fetchSummary: ["urlData"]
});

export { Types , Creators }