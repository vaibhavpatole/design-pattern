const fetchShowList = 'fetchShowList'
const fetchShowSummary = 'fetchShowSummary'

export default {
  fetchShowList,
  fetchShowSummary
}